import scipy.sparse as sp
import numpy as np
import skcuda.autoinit
import pycuda.gpuarray as gpuarray
import pycuda.curandom as curand


class SPARSE_RBM:
    def __init__(self, data, n_vis, n_vis_c, n_vis_w, n_hid):
        self.n_vis, self.n_vis_c, self.n_vis_w, self.n_hid = n_vis, n_vis_c,
        n_vis_w, n_hid
        self.vis = data
        self.vis_c = self.vis[:, :self.n_vis_c]
        self.vis_w = self.vis_w[:, :self.n_vis_w]
        self.W = sp.csc_matrix(np.random.uniform(size=(self.n_vis, self.n_hid)))
        self.b = sp.csc_matrix(np.random.uniform(size=(self.n_hid,)))
        self.a = sp.csc_matrix(np.random.uniform(size=(self.n_vis,)))

    def vis_given_hidden(self, hidden):
        sp.csc_matrix(curand.binominal(1, p=self.))

    def hid_gien_vis(self, vis)

