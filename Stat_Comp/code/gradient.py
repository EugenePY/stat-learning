import theano
import theano.tensor as T
import numpy as np
from collections import OrderedDict


class loglikelihood(object):
    '''
    calculate the log likelyhood for each sample(can be applied on
    multi-dimension
    numpy array)
    in this data set we assume that the attributes are independnet. So the
    so the log likelyhood function is suming them up.

    '''
    def __init__(self, input, mu, sigma, alpha):
        self.params = [mu, sigma, alpha]
        type_er = [i for i, param in enumerate(self.params) if (type(param) !=
                                                                T.TensorType)]
        if sum(type_er) > 0:
            print 'variables must be tensor type, %s is not TensorType' % \
              " ".join(str(i) for i in type_er)
        self.x = input
        self.mu = mu
        self.sigma = sigma
        self.alpha = alpha

    def normal_dis(self, x, mu, sigma):
        return (1/(T.sqrt(np.pi*2)*sigma))*T.exp(-(((x-mu)/sigma)**2)/2)

    def student_t(self, v, x):
        return T.log(T.gamma((v + 1) / 2) / T.sqrt(v * T.basic.pi)
                     (1 + x**2/v)**(-(v + 1) / 2))

    def ob_function(self):
        return T.sum(T.log(T.sum(self.normal_dis(self.x, self.mu, self.sigma) *
                                 [self.alpha, 1-self.alpha], 1)), 0)


class gradient_ascent(loglikelihood):

    def __init__(self, x, mu, sigma, alpha, lr=0.01):
        # method is including Momentumn and adaptive
        loglikelihood.__init__(self, x, mu, sigma, alpha)
        self.lr = lr

    def grad(self):
        grad = T.grad(self.ob_function(), self.params)
        return grad

    def method(self, method):
        '''
        the later term of gradient decent/ascent.
        theta_{t+1} = theta_{t} -/+ (later term)
        '''
        if method == 'momentumn':
            pass
        elif method == 'adaptive':
            pass
        elif method == 'wedecay':
            pass
        elif method == 'sgd':
            updates = OrderedDict()
            for param, gparam in zip(self.params, self.grad()):
                updates[param] = param + self.lr * gparam

        return updates

if __name__ == '__main__':
    print __file__
    cluster1 = np.random.standard_normal((100, 1))*10 + 5
    cluster2 = np.random.standard_normal((100, 1))*2 + 10
    test_data = np.vstack([cluster1, cluster2])
    batch_size = 99
    np.random.shuffle(test_data)
    test_data = theano.shared(test_data, broadcastable=(0, 1))

    x = T.col('x')
    index = T.lscalar('index')
    mu = theano.shared(np.array([7., 4.]).reshape(1, 2), broadcastable=(1, 0))
    sigma = theano.shared(np.array([3., 5.]).reshape(1, 2),
                          broadcastable=(1, 0))
    alpha = theano.shared(0.7)
    mle_grad = gradient_ascent(x, mu, sigma, alpha)
    nor = mle_grad.normal_dis(x, mu, sigma)
    f = theano.function([], nor,
                        givens={x: test_data
                                }
                        )
    print f()

    cost = mle_grad.ob_function()

    updates = mle_grad.method('sgd')

    MLE_learn = theano.function([index], cost,
                                updates=updates,
                                givens={
                                    x: test_data[index*(batch_size):
                                                 (index + 1)*(batch_size)]
                                        },
                                name='MLE'
                                )
    n_epoch = 3000
    for i in xrange(n_epoch):
        cost = []
        for j in xrange(test_data.get_value(borrow=True).shape[0]):
            cost += [MLE_learn(j)]
        print 'epoch %i: cost %f' % (i, np.mean(cost)), \
              mle_grad.params[1].get_value()
