import numpy as np


class dynamic_prog:
    '''
    The function calculate \sum_{t}f(z_{t}, z_{t+1})the Hidden Markov Chain.
    The method is finding the optimal sequence of the binary
    states given a score function. The score map function can use a lambda
    map or method.
    Argument:

        score map : the score map.

        T : the lenth of the sequence.

    '''
    def __init__(self, vis, trans_prob=np.array(
            [[0.45, 0.55], [0.45, 0.55]])):

        self.vis = vis
        self.trans_prob = trans_prob
        self.p0 = np.array([0.5, 0.5])
        self.Pa = 0.6
        self.Pb = 0.3
        self.B = []
        self.A = 0
        self.P = np.array([[self.Pa, 1-self.Pa],
                           [self.Pb, 1-self.Pb]])

    def dynamic_B_backward(self, t):
        def state_to_idx(state):
            return state - 1
        if t == (self.vis.shape[0] - 1):
            return self.p0 * self.P[:, self.vis[t][0]]
        else:
            PX = np.dot(self.trans_prob.T, self.dynamic_B_backward(t+1)) * \
                self.P[:, self.vis[t][0]]
            self.B += [PX.argmax()]
            self.A += np.log(PX.max())
            return PX

    def dynamic_A(self, t):
        def state_to_idx(state):
            return state - 1

        if t < 1:
            return self.p0 * self.P[:, self.vis[t][0]]
        else:
            PX = np.dot(self.trans_prob.T, self.dynamic_A(t-1)) * \
                self.P[:, self.vis[t][0]]
            self.B += [PX.argmax()]
            self.A += np.log(PX.max())
            return PX


if __name__ == '__main__':
    fake_data = np.random.binomial(1, p=0.5, size=(50, 2))
    method = dynamic_prog(fake_data)
    print method.dynamic_A(40)
    print method.dynamic_B_backward(42)
