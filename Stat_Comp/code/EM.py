import numpy as np
import scipy.stats as stats
import scipy.optimize as opt


class score_fun(object):
    def __init__(self, params):
        self.params = params

    def likelihood(self, x, alpha, params):
        mu, sigma, mu1, sigma1 = params
        return np.sum(alpha * stats.norm.logpdf(x, loc=mu, scale=sigma) +
                      (1 - alpha) * stats.norm.logpdf(x, loc=mu1, scale=sigma1))


class ExpectaionMaximum(score_fun):
    '''
    This Class impliment the simple EM for the HW3,4,5
    Base Problem, Clustering the two mean with different variance.
    Underlying distribution is two independent Normals.
    '''
    def __init__(self, data, **kargv):
        score_fun.__init__(self, np.random.normal(loc=0.01, size=(4,)).tolist())
        self.data = data
        self.state = np.random.binomial(1, p=0.5, size=data.shape)
        self.score = self.likelihood(self.params)
        self.max_iter = 1000
        self.likelihood_hist = []

    def likelihood(self, params):
        return -score_fun(self).likelihood(self.data, self.state, params)

    def mle(self):
        '''
        The mle function can be modified with a better solver, or use hand-build
        with Theano. We use Scipy for base-line.
        Do not use this method.
        '''

        res = opt.minimize(self.likelihood, self.params, method='nelder-mead',
                           options={'xtol': 1e-8, 'disp': True})
        return res

    def weight(self, data):
        stats.norm.logpdf(data)/stats.norm.logpdf(data)
        pass

    def mle_given_alpha(self):

        pass

    def mle_gievn_state(self):
        '''
        Given the states, we can calculate the mle estimator.
        Clustering problem
        k = 2,
        '''
        state_set = set(self.state)
        res = []

        for i in state_set:
            res += [self.dataset[self.state == i].mean()]
            res += [self.dataset[self.state == i].std()]

        return res

    def select(self):
        '''
        TODO:
        The function perform the selection percedure of the EM(for count data)
        '''
        for i in range(self.max_iter):
            if abs(self.cost_hist[-2] - self.cost_hist[-1]) < 1e-12:
                break

        pass

if __name__ == "__main__":
    data = np.hstack([np.random.normal(loc=50., scale=1, size=(20,)),
                      np.random.normal(loc=20, scale=2, size=(100,))])
