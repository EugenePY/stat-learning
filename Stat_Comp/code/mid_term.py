import numpy as np
import scipy.stats as ss
import matplotlib.pyplot as plt

from scipy.stats import laplace
# import MHasting algorithm
from Metropolis_Hastings import MHasting

# 1


def F_inv(X):
    '''
    Input X: (uniform)
    output : (double_exp)
    '''
    # this function generate the inversed CDF to
    # generate the samples from laplace dis from
    # uniform(0, 1)
    return np.sign(X-0.5)*np.log(1-2*np.abs(X-0.5))

method1 = F_inv(np.random.uniform(size=1000))
print method1.var()
print method1.mean()
dis = laplace
ax1 = plt.subplot(221)
res = ss.probplot(method1, dist='laplace', plot=plt)
ax1.set_title("Uniform")
# 2
# We have two ideas about how to sample laplace
# from t(v=3) we found it is hard ot find a proper C
# so we use MHasting method.


class MH_t(MHasting):
    def __init__(self, target):
        MHasting.__init__(self, target)
        self.size = (1000,)
        self.init = self.rng.standard_t(df=3,
                                        size=self.size)

    def proposal_sample(self, x):
        return self.rng.standard_t(df=3,
                                   size=self.size) + x

# target is the Target pdf (callable object)
target = ss.laplace.pdf
mhasting = MH_t(target)
method2 = mhasting.burning(100)  # we use 100 for burn-in steps
print method2.var()
print method2.mean()
print np.random.laplace(size=1000).var()

ax2 = plt.subplot(222)
res = ss.probplot(method2, dist='laplace', plot=plt)
ax2.set_title("MH student(v=3)")

class MH_random_walk(MHasting):
    def __init__(self, target):
        MHasting.__init__(self, target)
        self.size = (1000,)
        self.init = self.rng.normal(size=self.size)

    def proposal_sample(self, x):
        return self.rng.normal(size=self.size) + x

mhasting2 = MH_random_walk(target)
method3 = mhasting2.burning(100)
print method3.var()
print method3.mean()

ax3 = plt.subplot(223)
res = ss.probplot(method3, dist='laplace', plot=plt)
ax3.set_title("MH Normal")


class Slide_sampling(object):
    def __init__(self, n_sample):
        self.size = n_sample
        self.init = np.random.laplace(size=1)

    def uniform(self, L, U):
        return np.random.uniform(U, L, size=1)

    def LU_given_x(self, x):
        return 0, ss.laplace.pdf(x)

    def LU_given_y(self, y):
        return np.log(2*y), - np.log(2*y)

    def sample_y_given(self, x):
        L, U = self.LU_given_x(x)
        return self.uniform(L, U)

    def sample_x_given(self, y):
        L, U = self.LU_given_y(y)
        return self.uniform(L, U)

    def one_step(self, x):
        y = self.sample_y_given(x)
        x1 = self.sample_x_given(y)
        return x1

    def sampling(self):
        sample = [self.init]
        for i in range(self.size):
            sample += [self.one_step(sample[-1])]
        return np.array(sample)[1:].ravel()

method4 = Slide_sampling(n_sample=1000).sampling()
print method4.mean()
print method4.var()
ax3 = plt.subplot(224)
res = ss.probplot(method4, dist='laplace', plot=plt)
ax3.set_title("Slice Sampling")

############## section 2 ##############
 # 1.
print ''
sample = np.random.laplace(scale=1./100, size=1000000)
print (np.sin(sample)*200).mean()

# 2.
sample = np.abs(np.random.normal(scale=np.sqrt(1./100), size=10000))
mean = (np.exp(-50*sample**2.01 + 50*sample**2)*np.log(sample) * np.sqrt(np.pi)/100).mean()
print mean
# 3.
sigmas = 0.5*(1./np.append(1, np.arange(1, 20)) + 1./np.arange(1, 21))
sigmas = np.sqrt(sigmas.reshape(1, 20))
sample = np.abs(np.random.normal(scale=np.sqrt(sigmas),
                                 size=(10000000, 20)))
x_k = sample[:, :-1]
x_k1 = sample[:, 1:]
k = np.arange(1, 20).reshape(1, 19)
A = np.prod(sigmas**2)
B = np.sqrt(2*np.pi)
C = np.prod((np.cos(x_k) + k**2 * np.sin(x_k1)), 1).mean()
mean = A*B*C
print mean
############ section 3 #############

