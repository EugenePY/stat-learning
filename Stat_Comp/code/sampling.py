import numpy as np

# sample U(0,1)


def rand(n, seed):
    random = []
    for i in xrange(1, n + 1):
        a = ((i * (seed-1) % seed)/float(seed)) * 100
        random += [a - int(a)]
    return random

# sample Two Normal form U(0, 1)

def Normal(n):
    theta = (np.random.rand(n/2) * np.pi *2) - np.pi
    sample = np.random.rand(n/2)
    r = np.sqrt(-2 * np.log(1-sample))
    return np.array([r * np.cos(theta), r * np.sin(theta)])
# sample from Cauchy
def Normal_Cauchy(n):
    sample = []
    def norm_pdf(x):
        return 1/(np.sqrt(2*np.pi))*np.exp(-x**2/2)

    while len(sample) == n:
        np.random.standard_cauchy(1)
# Monte Carlo integral
# abs(cos(x))*exp(-50*x**2) we would sample many value which will take a small
# portion of the inergral we use important sampling to approximate the
# intergral.
if __name__ == "__main__":
    print rand(10, seed=19)
    print Normal(100)
