import numpy as np
import scipy.stats as stats

from dynamic_prog import dynamic_prog


class HMM(dynamic_prog):
    def __init__(self, vis):
        dynamic_prog.__init__(self, vis)
        self.vis = vis
        alpha, beta = np.random.uniform(0, 1, size=(2,)).tolist()
        self.trans_prob = np.array([[1-alpha, alpha], [beta, 1-beta]])
        self.hid_states = np.random.binomial(1, p=0.5, size=vis.shape)

    def E_step(self):
        pass

    def prob_zt_given_zt_last(self, t):
        '''
        parameter:
            t is the P(Z_{t}|Z_{t-1}) which we want to compute.
        output:
            a size of (n_hidden_state, n_hidden__state) in Hw's case is 2 by 2.
        '''
        pass
if __name__ == "__main__":
    f = open('../data/data_3.txt')
    x = np.loadtxt(f).T
    print HMM(x).P
