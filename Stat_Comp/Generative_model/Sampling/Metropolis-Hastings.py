import numpy as np


class MHasting(object):
    '''
    ############### Discrete Version ##############

    We want to sample a probability by a proposal distribution.
    Parameters:
        Sketch the algorithm:
            if r >= 1, x accpted x, otherwise
        Targrt dis: pi
        proposal:
    '''

    def __init__(self, target, n_vis=1, batch_size=1, rng=None):
        '''
        pi: a random stream object (state,) map random state to probability
        (pdf / pmf). It's a mapping.
        '''
        self.pi = target
        self.size = (batch_size, n_vis)

        if rng is None:
            self.rng = np.random.RandomState()

        self.init = self.rng.binomial(1, p=0.5, size=self.size)

    def proposal_sample(self, x):
        '''
        can be override by inherent.
        '''
        return self.rng.binomial(1, p=0.5, size=self.size)

    def MH_accept(self, y, x):
        r = self.pi(y)/self.pi(x)
        next_accept = r > self.rng.uniform(size=self.size)
        accept = np.vstack([bool(1-next_accept), next_accept])
        return accept

    def switch(self, y, x, accept):
        X = np.vstack([x, y])
        return X[accept]

    def accept_rate(self, accept):
        return accept[-1].mean()

    def update(self, burn_in=500):
        last_step = self.init
        next_step = self.proposal_sample(last_step)
        mean_accept_rate = []
        for step in xrange(burn_in):
            accept = self.MH_accept(next_step, last_step)
            mean_accept_rate += [self.accept_rate(accept)]
            last_step = self.switch(next_step, last_step, accept)
            next_step = self.proposal_sample(last_step)
        print "Accept rate: %.4f" % (np.mean(mean_accept_rate))
        return last_step


class MH_normal(MHasting):
    """
    ############## Continous Version ##############

    """
    def __init__(self, target):
        MHasting.__init__(self, target)
        self.init = self.rng.normal(size=self.size)

    def proposal_sample(self, x):
        return self.rng.normal(loc=x, size=self.size)


if __name__ == "__main__":

    def ber(x):
        event = x
        return (0.7**event)*(0.3**(1-event))

    def double_exp(x):
        return np.exp(-abs(x))*0.5

    model = MH_normal(double_exp)
    temp = []
    for i in range(500):
        temp += [model.update().tolist()[0]]
    P = double_exp(np.random.laplace(loc=0, size=(500,)))
    temp = double_exp(np.array(temp))
    print P, temp
    print (P*np.log(P)-P*np.log(np.array(temp))).mean()
