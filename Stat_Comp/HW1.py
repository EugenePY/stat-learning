import sys, os

import timeit
import scipy.io
import theano
import theano.tensor as T
import scipy
import numpy as np 

mat = scipy.io.loadmat('./data/data_1.mat')
Data = np.vstack([mat['height'], mat['weight']])
## gradient assent first by calculate the grad by theano. 
## Second by hand craft
x = T.matrix('x')


mu = theano.shared(np.mean(Data, 0), name='mu') # use mean to initiate
sigma = theano.shared(np.var(Data, 0), name='sigma') # use simple variance to 
                                                     # to initiate the sigma.                                                     
deg_freedom = theano.shared(np.mean(Data, 0), name='df')

def loglikelihood(x, mu, sigma, df):
    # calculate the log likelyhood for each sample(can be applied on multi-dimension
    # numpy array)
    # in this data set we assume that the attributes are independnet. So the 
    # so the log likelyhood function is suming them up.

    def normal_dis(x, mu, sigma):
        return 1/T.sqrt(np.pi*2)*T.exp(-(1/2)*((x-mu)/sigma)**2)

    def student_t(x, v):
        return T.gammav((v+1)/2)/T.sqrt(v*T.basic.pi)(1 + x**2/v)**(-(v+1)/2)
    
    def distribution(x):
        # return symbolic pdf
        return 

    return T.sum(T.log(self.distribution(x, mu, sigma, df)))

def hand_craft_grad(x):
    
    def hand_normal(x):
        pass
    def hand_student(x):
        pass
    return 

# we train grad ascent in 1000 steps
# for each attribute and get the best estimate of parameters we
# doing iter in scan function just the regular for loop but using gpu.

# Summary: the calculation of gradient is very difficult and tidious and the 
# performance is not as good as sybolic calculated grad(performace is evaluated
# by time took for eacho iter)

class grad_ascent(object):

    def __init__(self,model, batch_size, learning_rate, momentum, wedecay, n_iter=5000):
        pass
    def __iter__(self):
        for i in xrange(self.n_iter)


if __name__ == '__main__':
    x = T.matrix('x')
    f = xi*np.pi
    out=theano.function([x], f)
    print out(Data)    

